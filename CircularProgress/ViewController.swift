//
//  ViewController.swift
//  Created by Prahlad Reddy on 04/06/20.
//  Copyright © 2018 Prahlad Reddy. All rights reserved.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    
    var minValue = 0
    let maxValue = 100
    var downloader = Timer()
    
    @IBOutlet weak var downloadLabel: UILabel!
    @IBOutlet weak var downloadPercent: UILabel!
    @IBOutlet weak var downloadBar: UIProgressView!
    @IBOutlet weak var startButton: UIButton! {
        didSet {
            self.startButton.layer.borderWidth = 5
            self.startButton.layer.shadowColor = UIColor.white.cgColor
            self.startButton.layer.borderColor = UIColor.white.cgColor
            self.startButton.layer.cornerRadius = 20
        }
    }

   // @IBOutlet weak var CircularProgress: CircularProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
//        CircularProgress.trackColor = UIColor.white
//        CircularProgress.progressColor = UIColor.purple
//        CircularProgress.setProgressWithAnimation(duration: 4.0, value: 1)
        
    // Another CircularProgressView
//        let cp = CircularProgressView(frame: CGRect(x: 10.0, y: 10.0, width: 100.0, height: 100.0))
//        cp.trackColor = UIColor.white
//        cp.progressColor = UIColor(red: 252.0/255.0, green: 141.0/255.0, blue: 165.0/255.0, alpha: 1.0)
//        cp.tag = 101
//        self.view.addSubview(cp)
//        cp.center = self.view.center
//
//       self.perform(#selector(animateProgress), with: nil, afterDelay: 2.0)
        
        // Download Progress
    }

//    @objc func animateProgress() {
//        let cP = self.view.viewWithTag(101) as! CircularProgressView
//        cP.setProgressWithAnimation(duration: 3.0, value: 1)
//
//    }
    
    // IBAction
    @IBAction func hasStartButtonPressed(_ sender: UIButton) {
        self.downloadLabel.text = "Downloading..."
        self.startButton.isEnabled = false
        downloader = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: (#selector (ViewController.updater)), userInfo: nil, repeats: true)
        downloadBar.setProgress(0, animated: false)
    }
    
    @objc func updater() {
        if minValue != maxValue {
            minValue += 1
            downloadPercent.text = "\(minValue)%"
            downloadBar.progress = Float(minValue) / Float(maxValue)
        } else {
            startButton.isEnabled = true
            minValue = 0
            downloader.invalidate()
            self.downloadLabel.text = "Download Completed"
        }
    }
}

